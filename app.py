# Flask look for app.py file as run file
from flask import Flask
from flask_smorest import Api
from resources.todo import blb as TodoBlueprint
app = Flask(__name__)

# This configs are required for a successfull setup with flask smorest

# PROPOGATE_EXCEPTION will generate exception created by flask_smorest extension
app.config["PROPOGATE_EXCEPTION"] = True
app.config["API_TITLE"] = "Todo App API Documentation"
app.config["API_VERSION"] = "v1"
app.config["OPENAPI_VERSION"] = "3.0.0"
app.config["OPENAPI_URL_PREFIX"] = "/"
app.config["OPENAPI_SWAGGER_UI_PATH"] = "swagger-ui"
app.config["OPENAPI_SWAGGER_UI_URL"] = "https://cdn.jsdelivr.net/npm/swagger-ui-dist/"

# Connect flask and flask smorest
api = Api(app)

# Register routes
api.register_blueprint(TodoBlueprint)
