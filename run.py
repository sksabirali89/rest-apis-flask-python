# This is another way to start flask server
from flask import Flask

app = Flask(__name__)

# Change "app.get" to "app.route" to define a route for GET requests


@app.route("/todos")
def get_todos():
    return {"todos": "Get todo list"}


# This code will only run if this script is executed directly, not when imported as a module
if __name__ == '__main__':
    # Code here will execute when the script is run directly

    app.run(debug=True)
