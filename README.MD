# Build professional REST APIs with Python, Flask, Docker, Flask-Smorest, and Flask-SQLAlchemy

Set up the project locally

```

git clone https://gitlab.com/sksabirali89/rest-apis-flask-python.git
cd rest-apis-flask-python
python -m venv venv
For Windows .venv\Scripts\activate or for Mac/Linux source .venv/bin/activate

pip freeze > requirements.txt
python run.py
flask run --debug

```

Freeze dependencies in the requirements.txt

```
python -m venv venv
venv\Scripts\activate
pip install -r requirements.txt

```

```
docker build -t rest-apis-flask-python .
docker run -p 5005:5000 rest-apis-flask-python
docker run -dp 5005:5000 rest-apis-flask-python
```
