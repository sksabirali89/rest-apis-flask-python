from flask.views import MethodView
from flask_smorest import Blueprint, abort
from schemas import TodoSchema, TodoUpdateSchema

blb = Blueprint("todos", __name__, description="Todo routes")


@blb.route("/todos")
class Todos(MethodView):
    @blb.response(200, TodoSchema(many=True))
    def get(self):
        try:
            todos = [
                {
                    "id": "dfgdfg3434534dfgd",
                    "name": "Test",
                    "price": 0.12,
                    "store_id": "fsdfd234234fddfg"
                }
            ]
            return todos
            # return todos.values()
        except Exception as e:
            abort(500, message=str(e))

    @blb.arguments(TodoSchema)
    @blb.response(201, TodoSchema())
    def post(self, todo_data):
        try:
            return todo_data
        except Exception as e:
            abort(500, message=str(e))


@blb.route("/todos/<string:id>")
class Todos(MethodView):
    @blb.arguments(TodoUpdateSchema)
    @blb.response(200, TodoSchema())
    def put(self, id):
        try:
            # return "Update todo "+id
            todo = {
                "id": "dfgdfg3434534dfgd",
                "name": "Test",
                "price": 0.12,
                "store_id": "fsdfd234234fddfg"
            }

            return todo
        except Exception as e:
            abort(500, message=str(e))

    def delete(self, id):
        try:
            return "Delete todo "+id
        except Exception as e:
            abort(500, message=str(e))
