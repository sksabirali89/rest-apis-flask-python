from marshmallow import fields, Schema


class TodoSchema(Schema):
    # load_only=True does not return id in response
    id = fields.String(dump_only=True)
    name = fields.String(required=True)
    price = fields.Float(required=True)
    store_id = fields.String(required=True)


class TodoUpdateSchema(Schema):
    name = fields.String()
    price = fields.Float()
